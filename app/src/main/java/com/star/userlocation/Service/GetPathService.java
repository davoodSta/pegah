package com.star.userlocation.Service;

import android.util.Log;

import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.PolylineOptions;
import com.star.userlocation.CallBackInterface.ICallBackDirectionPointer;
import com.star.userlocation.Utils;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class GetPathService {

    private String TAG = "GetPathFromLocation";
    private String API_KEY = "AIzaSyCo-E8VWgugRJoAEwn64pMthsH8-GoDM9Y";
    private static final int PATTERN_GAP_LENGTH_PX = 10;  // 1
    private static final Gap GAP = new Gap(PATTERN_GAP_LENGTH_PX);
    private static final Dot DOT = new Dot();
    private static final List<PatternItem> PATTERN_DOTTED = Arrays.asList(DOT, GAP);

    public void GetPathService(LatLng source, LatLng destination, ICallBackDirectionPointer listener)
    {


        String str_origin = "origin=" + source.latitude + "," + source.longitude;
        String str_dest = "destination=" + destination.latitude + "," + destination.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + API_KEY;

        Log.i("GetPathServiceURL",url);

        new NetworkLayer().getRequest(url, null, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Log.i("GetPathServiceFailure",e.getMessage());
                String ErrorMessage="Connection not conected to the server";
                listener.directionPointerOnFailure(ErrorMessage);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String jsonData=response.body().string();

                Log.i("GetPathServiceResponse",jsonData);

                if (Utils.isJSONValid(jsonData))
                {
                    try {
                        JSONObject result = new JSONObject(jsonData);
                        if (result.getString("status").equals("OK")) {
                            PolylineOptions polylineOptions = Utils.ConvertToPolyLine(jsonData);
                            polylineOptions.pattern(PATTERN_DOTTED);
                            listener.directionPointerOnPath(polylineOptions,destination.hashCode());
                        }
                        else if (result.getString("status").equals("ZERO_RESULTS"))
                        {
                            String ErrorMessage="no route could be found between you and destination";
                            listener.directionPointerOnZeroResult(ErrorMessage);
                        }
                        else if (result.getString("status").equals("NOT_FOUND"))
                        {
                            String ErrorMessage="at least one of the locations specified in the request's origin, destination, or waypoints could not be geocoded";
                            listener.directionPointerOnNotFound(ErrorMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }
                else
                {
                    String ErrorMessage="Connection not conected to the server properly";
                    listener.directionPointerOnInvalidJson(ErrorMessage);
                }
            }
        });
    }



}
