package com.star.userlocation;

import com.google.android.gms.maps.model.LatLng;

public class Point {

    private LatLng latLng;

    private int tag;

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public Point(LatLng latLng, int tag) {
        this.latLng = latLng;
        this.tag = tag;
    }
}
