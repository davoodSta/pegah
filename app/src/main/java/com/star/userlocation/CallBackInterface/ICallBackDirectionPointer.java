package com.star.userlocation.CallBackInterface;

import com.google.android.gms.maps.model.PolylineOptions;

public interface ICallBackDirectionPointer {

    void directionPointerOnPath(PolylineOptions polyLineOptions, int hashCode);

    void directionPointerOnFailure(String ErrorMessage);

    void directionPointerOnInvalidJson(String ErrorMessage);

    void directionPointerOnZeroResult(String ErrorMessage);

    void directionPointerOnNotFound(String ErrorMessage);

}
