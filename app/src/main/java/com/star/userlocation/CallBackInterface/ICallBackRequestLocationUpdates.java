package com.star.userlocation.CallBackInterface;

import android.location.Location;
import android.os.Bundle;

public interface ICallBackRequestLocationUpdates {

    void onLocationChanged(Location location);
}
