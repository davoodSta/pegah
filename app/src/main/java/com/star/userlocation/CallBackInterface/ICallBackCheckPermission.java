package com.star.userlocation.CallBackInterface;

public interface ICallBackCheckPermission {

    void onPermissionGranted();

}
