package com.star.userlocation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.star.userlocation.CallBackInterface.ICallBackCheckPermission;
import com.star.userlocation.CallBackInterface.ICallBackRequestLocationUpdates;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Utils {

    public static void checkPermission(Activity activity, String permission, int requestCode, ICallBackCheckPermission listener)
    {

        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(
                activity,
                permission)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat
                    .requestPermissions(
                            activity,
                            new String[] { permission },
                            requestCode);
        }
        else
        {
            listener.onPermissionGranted();
        }
    }

    @SuppressLint("MissingPermission")
    public static void getCurrentLocation(Context context, ICallBackRequestLocationUpdates listener)
    {

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 1, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                listener.onLocationChanged(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });

    }

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static PolylineOptions ConvertToPolyLine(String jsonstring)
    {

        String TAG="ConvertToPolyLine";
        PolylineOptions lineOptions = null;
        try {
            JSONObject jsonObject = new JSONObject(jsonstring);
            List<List<HashMap<String, String>>> routes = null;
            DirectionHelper helper = new DirectionHelper();
            routes = helper.parse(jsonObject);
            Log.e(TAG, "Executing Routes : "/*, routes.toString()*/);


            ArrayList<LatLng> points;

            // Traversing through all the routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = routes.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLUE);

                Log.e(TAG, "PolylineOptions Decoded");
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        // Drawing polyline in the Google Map for the i-th route
        if (lineOptions != null) {
            return lineOptions;
        } else {
            return null;
        }
    }

    public static void addMarker(GoogleMap googleMap, LatLng latLng)
    {
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_marker);
        googleMap.addMarker(new MarkerOptions().position(latLng).icon(icon));
    }

    public static Marker addMyLocation( GoogleMap googleMap, LatLng latLng)
    {
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);
        return googleMap.addMarker(new MarkerOptions().position(latLng).icon(icon));
    }

    public static Polyline addPolyLine(GoogleMap googleMap, PolylineOptions polylineOptions)
    {
        return googleMap.addPolyline(polylineOptions);

    }

    public static void vibrateDevice(Context context)
    {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(30);
        }
    }

}
