package com.star.userlocation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.star.userlocation.CallBackInterface.ICallBackCheckPermission;
import com.star.userlocation.CallBackInterface.ICallBackDirectionPointer;
import com.star.userlocation.CallBackInterface.ICallBackRequestLocationUpdates;
import com.star.userlocation.R;
import com.star.userlocation.Service.GetPathService;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback ,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener, ICallBackDirectionPointer {

    private static GoogleMap gMap;
    private Marker marker;
    private ArrayList<Polyline> polylines;
    private static GoogleApiClient googleApiClient;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 101;
    private static String TAG = "MainActivity";
    private Point point;
    private static LatLng source;
    private ArrayList<Point> destinations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitializeMap();
        setUpGClient();

        destinations = new ArrayList<>();
        polylines = new ArrayList<>();

    }

    private void InitializeMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.mapview);
        mapFragment.getMapAsync(this);
    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                .enableAutoManage(MainActivity.this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;

        gMap.setOnMapClickListener(this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Utils.checkPermission(this, Manifest.permission.ACCESS_FINE_LOCATION, LOCATION_PERMISSION_REQUEST_CODE, new ICallBackCheckPermission() {
            @Override
            public void onPermissionGranted() {
                Utils.getCurrentLocation(getApplicationContext(), new ICallBackRequestLocationUpdates() {
                    @Override
                    public void onLocationChanged(Location location) {
                        source = new LatLng(location.getLatitude(),location.getLongitude());
                        if (marker != null)
                        {
                            marker.remove();
                        }
                        marker = Utils.addMyLocation(gMap,new LatLng(location.getLatitude(),location.getLongitude()));

                        drawPath();
                    }
                });
            }
        });
    }

    private void drawPath()
    {
        for (int i=0;i<destinations.size();i++)
        {
            new GetPathService().GetPathService(source,destinations.get(i).getLatLng(),this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //granted
                    Utils.getCurrentLocation(this, new ICallBackRequestLocationUpdates() {
                        @Override
                        public void onLocationChanged(Location location) {
                            source = new LatLng(location.getLatitude(),location.getLongitude());
                            if (marker != null)
                            {
                                marker.remove();
                            }
                            marker = Utils.addMyLocation(gMap, source);
                        }
                    });
                } else {
                    //not granted

                    String Message="For Get Device Location you must allow Location Permission.";
                }
                break;
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {

        Utils.addMarker(gMap,latLng);
        point = new Point(latLng,latLng.hashCode());
        destinations.add(point);
    }

    @Override
    public void onMapLongClick(LatLng latLng) {



    }

    @Override
    public void directionPointerOnPath(PolylineOptions polylineOptions, int hashCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                for (int i=0; i<polylines.size();i++)
//                {
//                    if (polylines.get(i) != null)
//                    {
//                        polylines.get(i).remove();
//                    }
//                }
                for (int i = 0; i<destinations.size(); i++)
                {
                    if (destinations.get(i).getTag() == hashCode)
                    {
                        if(polylines.size()>0)
                        {
                            polylines.get(i).remove();
                        }
                    }
                }
                polylines.add(Utils.addPolyLine(gMap,polylineOptions));
            }
        });
    }

    @Override
    public void directionPointerOnFailure(String ErrorMessage) {

    }

    @Override
    public void directionPointerOnInvalidJson(String ErrorMessage) {

    }

    @Override
    public void directionPointerOnZeroResult(String ErrorMessage) {

    }

    @Override
    public void directionPointerOnNotFound(String ErrorMessage) {

    }
}
